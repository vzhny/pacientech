import React from 'react';
import Hero from '@/components/Hero/Hero';
import styles from './Landing.module.scss';

const Landing = () => (
  <div className={styles.Landing}>
    <Hero size="full-height">
      <h1>Landing Hero</h1>
    </Hero>
  </div>
);

export default Landing;
