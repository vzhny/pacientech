import React, { Component } from 'react';
import { Router } from '@reach/router';
import styles from './App.module.scss';
import Navbar from '@/components/Navbar/Navbar';
import Landing from '@/pages/Landing/Landing';
import NotFound from '@/pages/NotFound/NotFound';

export default class App extends Component {
  render() {
    return (
      <div className={styles.Container}>
        <Navbar />
        <Router>
          <Landing path="/" />
          <NotFound default path="/*" />
        </Router>
      </div>
    );
  }
}
