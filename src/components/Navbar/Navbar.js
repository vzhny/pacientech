import React from 'react';
import { navigate } from '@reach/router';
import MainLogo from '@/images/pacientech_logo.png';
import styles from './Navbar.module.scss';
import NavbarNavigation from '@/components/NavbarNavigation/NavbarNavigation';

const redirectToLanding = () => {
  navigate('/');
};

const Navbar = () => (
  <nav className={styles.Navbar}>
    <img className={styles.Branding} src={MainLogo} onClick={redirectToLanding} alt="Main Pacientech Logo" />
    <NavbarNavigation />
  </nav>
);

export default Navbar;
