import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import styles from './Modal.module.scss';

const Modal = ({ clicked, children }) => (
  <>
    <div className={styles.Backdrop} onClick={clicked} />
    <div className={styles.Modal}>
      <Card title="Add Link">{children}</Card>
    </div>
  </>
);

Modal.propTypes = {
  clicked: PropTypes.func.isRequired,
  linksUpdated: PropTypes.func.isRequired,
};

export default Modal;
