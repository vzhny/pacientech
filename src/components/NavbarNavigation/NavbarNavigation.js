import React from 'react';
import styles from './NavbarNavigation.module.scss';

const NavbarNavigation = () => (
  <ul className={styles.NavbarNavigation}>
    <li>
      <a href="#">Log In</a>
    </li>
    <li>
      <a href="#">Log Out</a>
    </li>
    <li>
      <a href="#" className={styles.RegisterButton}>
        Register
      </a>
    </li>
  </ul>
);

export default NavbarNavigation;
