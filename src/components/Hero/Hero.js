import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './Hero.module.scss';

const Hero = ({ size, children }) => {
  const classNames = cn(styles.Hero, {
    [styles.Small]: size === 'sm',
    [styles.Medium]: size === 'md',
    [styles.Large]: size === 'lg',
    [styles.FullHeight]: size === 'full-height',
  });

  return <section className={classNames}>{children}</section>;
};

Hero.propTypes = {
  size: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
};

export default Hero;
