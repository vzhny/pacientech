import React from 'react';
import CardHeader from '@/components/Card/CardHeader/CardHeader';
import CardBody from '@/components/Card/CardBody/CardBody';
import styles from './Card.module.scss';

const Card = ({ title, children }) => (
  <div className={styles.Card}>
    <CardHeader title={title} />
    <CardBody>{children}</CardBody>
  </div>
);

export default Card;
